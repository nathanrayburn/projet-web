<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

if(!isset($_SESSION["userEmail"]))
{
    $accountButtonName = "Login";
    $accountActionName = "login";
    $accountText = "Vous n'êtes pas connecté(e) !";
}
else
{
    $accountButtonName = "Logout";
    $accountActionName = "logout";
    $accountText = "Bonjour ".$_SESSION["userEmail"]." !";
}



?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">



    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/grayscale.min.css" rel="stylesheet">

</head>

<body id="page-top">
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">

       <a class="navbar-brand js-scroll-trigger" href="#page-top">EasySearch</a>

        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarMenu" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarMenu">
            <ul class="navbar-nav ml-auto">





                <?php

                if(isset($_GET['action'])) { $action = $_GET['action'];}else{$action='home';}


                switch ($action) {
                    case 'home':
                        if (!isset($_SESSION["userEmail"])){
                            echo '
                                              <li> <a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a></li>
                                                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=login">Login</a>
                                            
                                            </li>';


                        }else{
                            echo '<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="index.php?action=profil">' .$_SESSION["firstname"].' '.$_SESSION["lastname"].' </a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=annonces">Annonces</a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=logout">Logout</a></li>';


                        }

                        break;
                    case 'annonces':
                        if (!isset($_SESSION["userEmail"])){
                            echo '
                                              <li> <a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a></li>
                                                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=login">Login</a>
                                            
                                            </li>';


                        }else{
                            echo '<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="index.php?action=profil">' .$_SESSION["firstname"].' '.$_SESSION["lastname"].' </a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=logout">Logout</a></li>';


                        }

                        break;
                    case 'login':
                        if (!isset($_SESSION["userEmail"])){
                            echo '
                                                <li><a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a></li>
                                                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=signup">Sign Up</a>
                                            
                                            </li>';


                        }else{
                            echo '<li class="nav-item"> <a class="" href="index.php?action=profil">' .$_SESSION["firstname"].' '.$_SESSION["lastname"].' </a></li>
                                                      <li class="nav-item"><a class="" href="index.php?action=annonces">Annonces</a></li>
                                                      <li class="nav-item"><a class="" href="index.php?action=logout">Logout</a></li>';


                        }
                        break;
                    case 'signup':
                        if (!isset($_SESSION["userEmail"])){
                            echo '
                                             <li>   <a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a></li>
                                                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=login">Login</a>
                                            
                                            </li>';


                        }else{
                            echo '<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="index.php?action=profil">' .$_SESSION["firstname"].' '.$_SESSION["lastname"].' </a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=annonces">Annonces</a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=logout">Logout</a></li>';


                        }
                        break;
                    case 'logout':
                        if (!isset($_SESSION["userEmail"])){
                            echo '<li>
                                   <a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a></li>
                                                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=login">Login</a>
                                            
                                            </li>';


                        }else{
                            echo '<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="index.php?action=profil">' .$_SESSION["firstname"].' '.$_SESSION["lastname"].' </a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=annonces">Annonces</a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=logout">Logout</a></li>';


                        }
                        break;
                    default :
                        if (!isset($_SESSION["userEmail"])){
                            echo '
                                                <a class="nav-link js-scroll-trigger" href="index.php?action=home">Home</a>
                                                <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=login">Login</a>
                                            
                                            </li>';


                        }else{
                            echo '<li class="nav-item"> <a class="nav-link js-scroll-trigger" href="index.php?action=profil">' .$_SESSION["firstname"].' '.$_SESSION["lastname"].' </a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=annonces">Annonces</a></li>
                                                      <li class="nav-item"><a class="nav-link js-scroll-trigger" href="index.php?action=logout">Logout</a></li>';


                        }
                }

                ?>
           </ul>
        </div>
    </div>
</nav>




    <!-- Header -->
    <header id="headtop" class="masthead">
        <div class="container d-flex h-100 align-items-center">
            <div class="mx-auto text-center">
                <?=$contenu;?>
            </div>
        </div>
    </header>


    <!-- Footer -->
    <footer class="bg-black small text-center text-white-50">

        <div class="container">
            Copyright &copy; Nathan Rayburn 2019
        </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/grayscale.min.js"></script>

</body>

</html>
