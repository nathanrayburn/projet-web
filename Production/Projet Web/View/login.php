<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre = "Login";

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">


        <!-- Bootstrap core CSS -->
        <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom fonts for this template -->
        <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
              rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="../css/grayscale.min.css" rel="stylesheet">
        <title>Login</title>
    </head>

    <body id="page-top">


    <form method="post" name="formLogin" action="index.php?action=login">
        <div class="container text-white">


            <div class="container">

                <div class="card text-dark">
                    <div class="justify-content-md-center">
                        <i class="icon fas fa-user h2"> Login</i>
                    </div>

                    <div class="col justify-content-md-center">
                        <div class=" col-auto">
                            <input name="inputEmail" class="form-control" type="email" id="inputEmail"
                                   aria-describedby="emailHelp" placeholder="E-mail address" required>
                        </div>
                        <div class="col-auto">
                            <input name="inputpassword" class="form-control" type="password" id="inputPassword"
                                   placeholder="Password" required>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-outline-dark col-auto" type="submit">Submit</button>
                            <div class="col-auto"><p>Don't have an account ?<a class="text-danger"
                                                                               href="index.php?action=signup">Sign
                                        Up</a></p></div>
                            <?php
                            if (isset($_SESSION["ErrorLogin"])) {
                                echo '<div class="col-auto"><p class="col-auto">Your E-mail or password is invalid *</p></div>';
                            }
                            ?>

                        </div>


                    </div>
                </div>
            </div>
        </div>


    </form>


    </body>

    </html>
<?php
$contenu = ob_get_clean();
require "gabarit.php";