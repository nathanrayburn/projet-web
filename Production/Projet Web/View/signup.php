
<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre="Signup";

?>
<!DOCTYPE html>
<html lang="en">


</head>

<body id="page-top">


<div class="container d-flex h-100 align-items-center text-white ol-md-4 col-md-offset-6">

    <form method="post" name="formRegister" action="index.php?action=signup">
            <div class="container">

                 <div class="card text-dark">

                     <div class="col-auto">
                         <i class="icon fas fa-user h2"> Sign Up</i>
                     </div>
                    <div class="col-auto">
                        <div class="form-group col-auto">
                            <input name="inputFirstName" class="form-control" type="text" id="inputEmail"  placeholder="First Name" required>
                        </div>
                        <div class="form-group col-auto">
                            <input name="inputLastName" class="form-control" type="text" id="inputEmail"  placeholder="Last Name" required>
                        </div>
                        <table>

                            <ul>
                                <tr><td> <input  class="form-control" type="text" name="locality"  placeholder="City" required></td>
                                    <td><input  class="form-control" type="text" name="address"  placeholder="Address" required> </td>
                                </tr>
                                <tr>
                                    <td><input  class="form-control" type="text" name="zipcode"  placeholder="zip-code" required></td>
                                    <td><input  class="form-control" type="text" name="phonenumber"  placeholder="Phone Number" required></td>
                                </tr>
                            </ul>

                        </table>
                        <div class="form-group col-auto">
                            <input name="inputEmail" class="form-control" type="email" id="inputEmail" aria-describedby="emailHelp" placeholder="E-mail" required>
                        </div>
                        <div class="form-group col-auto">
                            <input name="inputPassword" class="form-control" type="password" id="inputPassword" placeholder="Password" required>
                        </div>
                        <div class="form-group col-auto">
                            <input name="inputPasswordCheck" class="form-control" type="password" id="inputPassword" placeholder="Repeat Password" required>
                        </div class="form-group">
                         <button class="btn btn-outline-dark col-auto" type="submit">create <i class="icon fas fa-unlock-alt"></i></button>
                    </div>
                     <div class="col-auto"><p>Already have an account?<a class="text-danger" href="index.php?action=login">Sign in</a></p></div>


                 </div>
            </div>



        </form>


</div>

</body>

</html>
<?php
$contenu = ob_get_clean();
require "gabarit.php";
