<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre = "Home";

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Nathan Rayburn">


    </head>

    <body id="page-top">
    <!-- Search Section -->
    <div class="container">


    <!-- Projects Section -->
    <section id="projects" class="img-fluid">


            <p class="h2 centered">Annonces Récentes</p>
        <div class="row justify-content-md-center">
        <?php

        foreach($Recents as $advert){
            $image = $advert["image"];
            $title = $advert["title"];
            $description = $advert["description"];
            $advertID = $advert["advertId"];
        echo'

            <div class="col-md-4 mb-3 mb-md-0">

               <a href="index.php?action="> <h4 class="text-uppercase btn btn-outline-dark m-0">'.$title.'</h4></a>


                <div class="card py-4 h-100" style="max-height:350px;" >
                    <div class="card-body text-center">
                        <picture>


                            <div><img class="img-fluid" style="max-height: 150px" src = "'.$image.'"></div>



                        </picture>
                        <hr class="my-4">
                        <div class="small text-black-50">
                        
                            <a>'.$description.'</a>
                        </div>
                        
                    </div>
                    
                </div>
                
            </div>';
            }
            ?>



        </div>
    </section>
    </div>
    </body>

    </html>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
