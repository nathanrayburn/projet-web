<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre = "Post";

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Nathan Rayburn">

        <meta charset=utf-8 />
        <title>Advert</title>
        <!--[if IE]>
        <script src="js/img.js"></script>
        <![endif]-->
        <style>
            article, aside, figure, footer, header, hgroup,
            menu, nav, section { display: block; }
        </style>

    </head>

    <body id="page-top">
    <!-- Search Section -->
    <div class="container">
        <div class="card">

            <form method="post" name="formAdvert" action="index.php?action=addAdvert" enctype="multipart/form-data">>
                <p class="h2">Annonce</p>
                <div class="col-auto">
                    <label class="form-check-label">Title</label>
                    <input name="title" class="form-control" type="text">
                    <SELECT class="form-control" name="type" size="1" required>
                        <OPTION >Type</OPTION>
                        <OPTION>Location</OPTION>
                        <OPTION>Service</OPTION>
                        <OPTION>Vente</OPTION>
                    </SELECT>

                </div>

                <div class="col-auto">
                    <label class="form-check-label">Category</label>
                    <input name="category" class="form-control" type="text" required>
                    <label>Price</label>
                    <input name="price" class="form-control" type="number" required>
                </div>
            <div class="col-auto">
                <label class="form-check-label">Description</label>
                <textarea name="description" class="form-control" required></textarea>
            </div>
            <div class="col-auto">
                <input name="imgUpload" class="form-control" type='file' onchange="readURL(this);" />

            </div>

                <button class="btn btn-success" type="submit">Create</button>
            </form>

        </div>

    </div>
    </body>

    </html>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
