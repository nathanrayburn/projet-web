<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre = "Home";
$rows = 0;

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Nathan Rayburn">


    </head>

    <body id="page-top">
    <!-- Search Section -->
    <div class="container mb-3">


        <div class="yox-view">

            <?php


            foreach ($table as $result) : ?>
                <?php $rows++; ?>
               <div class="flex-row">
                    <ul class="thumbnails">
                    <?php $rows=0;?>


                    <li class="span3 card" style="max-height: 300px;max-width: 300px;">
                        <div class="thumbnail">
                            <a href="<?= $result['image']; ?>" target="blank"><img class="img-thumbnail" src="<?= $result['image']; ?>" alt="<?= $result['advertId']; ?>" ></a>
                            <div class="caption">
                                <h3><a href="index.php?action=details&code=<?= $result['advertId']; ?>"><?= $result['advertId']; ?></a></h3>
                                <p><strong>Marque : </strong><?= $result['title']; ?></p>
                                <p><strong>ModÃ¨le : </strong><?= $result['type']; ?></p>
                                <p><strong>Description </strong><?= $result['description']; ?> cm</p>
                                <p><strong>Price :</strong> CHF <?= $result['price']; ?>.- / jour</p>
                                <p><strong> : </strong><?= $result['category']; $result["type"]; ?></p>

                            </div>

                        </div>

                    </li>

                    </ul>
                    </div>

            <?php endforeach ?>

        </div>
    </div>
    </body>

    </html>

<?php
$contenuA = ob_get_clean();
require "gabarit_annonce.php";
