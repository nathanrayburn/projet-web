<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre = "Home";

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Nathan Rayburn">


    </head>

    <body id="page-top">




    <div class="row">

        <div class="card">
            <h2><?=$details["title"]?></h2>
            <img class="img-thumbnail" style="max-height: 300px;max-width: 300px;" src="<?=$details["image"]?>">
            <hr>
            <p><strong>Date & Details :</strong></p>
            <p><strong>Published date :</strong><?=$details["date"];?></p>
            <p><strong>Price :</strong> <?=$details["price"]?> CHF</p>
            <p><strong>Type and category :</strong><?=$details["type"]?>, <?=$details["category"]?></p><p></p>
            <p><strong>Description :</strong> <?=$details["description"]?></p>
        </div>
        <div class="card">
            <h2>Seller's Information</h2>
            <p><?=$details["firstname"];?></p>
            <p><?=$details["lastname"];?></p>
            <p><?=$details["mail"]?></p>
        </div>

    </div>

    <!-- Contact Section -->
    <section class="contact-section col-my-6">
        <form method="post" name="formLogin" action="index.php?action=mail">
        <div class="container">

            <div class="row">

                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-6">
                        <div class="card-body text-center">
                            <i class="fas fa-envelope text-primary mb-2"></i>
                            <h4 class="text-uppercase m-0">Email</h4>
                            <hr class="my-4">
                            <div class="small text-black-50">
                                <a href="#"><?=$details["mail"];?></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-6">
                        <div class="card-body text-center">
                            <i class="fas fa-mobile-alt text-primary mb-2"></i>
                            <h4 class="text-uppercase m-0">Phone</h4>
                            <hr class="my-4">
                            <div class="small text-black-50"><?=$details["phonenumber"];?></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0">
                    <div class="card py-6">
                        <div class="card-body text-center">
                            <div class="small text-black-50">
                                <input type="email" name="inputEmail" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" id="inputEmail" placeholder="Enter email address..." required>
                                <hr class="my-4">
                                <textarea name="inputMessage" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" placeholder="Need help? Enter your message here..." required></textarea>
                                <hr class="my-4">
                                <button type="submit" name="inputContact" class="btn btn-primary mx-auto">Send</button>   </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="social d-flex justify-content-center">

                <a href="#" class="mx-2">
                    <i class="fab fa-twitter"></i>
                </a>
                <a href="#" class="mx-2">
                    <i class="fab fa-facebook-f"></i>
                </a>
                <a href="#" class="mx-2">
                    <i class="fab fa-github"></i>
                </a>
            </div>

        </div>
    </form>
    </section>

    </body>

    </html>

<?php
$contenu = ob_get_clean();
require "gabarit.php";

