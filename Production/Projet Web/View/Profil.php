<?php
/**
 * Created by PhpStorm.
 * User: Pascal.BENZONANA
 * Date: 08.05.2017
 * Time: 09:16
 */

// tampon de flux stocké en mémoire

ob_start();
$titre = "Home";

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Nathan Rayburn">


    </head>

    <body id="page-top">
    <!-- Search Section -->
    <h1><?php echo $_SESSION["firstname"].' '.$_SESSION["lastname"]; ?></h1>
    <div class="container row justify-content-md-center">


        <div class="card col-auto">

        <h2 class="text-info">Mes information</h2>
            <hr>
        <table class="">
            <thead class="">
            <tr>
           </tr>
            <tr>
                <th scope="row">First name</th><td scope=""><?=$_SESSION['firstname'];?></td>
            </tr>
            <tr>
                <th scope="row">Last name</th><td scope=""><?=$_SESSION['lastname'];?></td>
            </tr>
            <tr>
                <th scope="row">Phone number</th><td scope=""><?=$_SESSION['phonenumber'];?></td>
            </tr>
            <tr>
                <th scope="row">City</th><td scope=""><?=$_SESSION['city'];?></td>
            </tr>
            <tr>
                <th scope="row">Zip Code</th><td scope=""><?=$_SESSION['zipcode'];?></td>
            </tr>
            <tr><th scope="row">Address</th><td scope=""><?=$_SESSION['address'];?></td></tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        </div>
        <div class="card col-auto">

            <h2 class="text-info text-center">Mes Annonces</h2>
            <hr>
            <table class=" col-auto">
                <thead class="">

                <tr>
                    <th scope="col">Title</th>
                    <th scope="col">Date</th>
                    <th scope="col">Type</th>
                    <th scope="col">Catégorie</th>
                    <th scope="col">Price</th>
                    <th scope="col">Image</th>

                </tr>
                </thead>
                <tbody class="table">




                <?php foreach ($userAdvert as $result) : ?>

                    <tr>
                        <td scope="row"><?=$result['title'];?></td>
                        <td scope="row"><?= $result['date']; ?></td>
                        <td scope="row"><?= $result['type']; ?></td>
                        <td scope="row"><?= $result['category']; ?></td>
                        <td scope="row"><?= $result['price']; ?></td>
                        <td scope="row"><img class="thumbnail" style ="max-height:50px;max-width:50px;" src="<?= $result['image']; ?>" alt="<?= $result['advertId']; ?>" ></td>
                        <td scope ="row"><a  href="index.php?action=modify&code=<?= $result['advertId']; ?>" class="btn btn-small">Modify</a> <a  href="index.php?action=delete&code=<?= $result['advertId']; ?>" class="btn btn-warning">Delete</a></td>
                    </tr>

                <?php endforeach ?>




                </tbody>
                <tr>

                </tr>
                <?php
                /* Affichage annonces dynamique
                 * foreach($)
                 */
                ?>
            </table>
            <a href="index.php?action=addAdvert" class="btn btn-success text-white">Add a new advert</a>
        </div>


    </div>
    </body>

    </html>

<?php
$contenu = ob_get_clean();
require "gabarit.php";
