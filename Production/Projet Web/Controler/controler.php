<?php
require "Model/userManagement.php";

//Add an Advert into the data base
/**
 * Add advert  with image verification
 * @param $Advert
 */
function addAdvert($Advert){
    if(isset($Advert["title"]) && isset($Advert["type"]) && isset($Advert["category"]) && isset($Advert["description"])){
        $title = $Advert["title"];

        $type = $Advert["type"];

        $category = $Advert["category"];

        $description = $Advert["description"];

        $dates = date("Y.n.d");

        $price = $Advert["price"];

        $target_dir = "img/assets/images/adverts/";

        $image = $target_dir;

        $target_file = $target_dir . basename($_FILES["imgUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
// Check file size
        if ($_FILES["imgUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["imgUpload"]["tmp_name"], $target_file)) {
                echo "The file ". basename( $_FILES["imgUpload"]["name"]). " has been uploaded.";
                uploadAdvert($dates,$title,$description,$category,$image,$price,$type);
                displayProfil();
            } else {

                require "View/post.php";
            }
        }

    }else{
        require "View/post.php";
    }

}
//Delete an Advert in the data base
function deleteAdvert(){
    $code = $_GET["code"];
    removeAdvert($code);
    $_GET["action"] = "profil";
    displayProfil();
}
//Modifiy an Advert
function modifyAdvert(){

}
//Send mail
function sendMail($mail){
    $userMail = $mail["inputEmail"];
    $userMessage = $mail["inputMessage"];
    mailUser($userMail,$userMessage);
    displayAnnonce();
}
//Display the detail page
function displayDetails(){
    $details = details();
    require "View/details.php";
    return $details;
}
//Display the Profil page
function displayProfil(){
    $userAdvert = userAdvert();
    require 'View/Profil.php';
    return $userAdvert;

}
//Display the Home Page with recents adverts
function displayHome()

{


    $Recents = advertsExport();
    $_GET["action"] = "home";
    require 'View/Form.php';
    return $Recents;
}
//Display Login with Login function included
function displayLogin($login)
{
    login($login);
    require "View/login.php";
    return $login;

}

/**
 * Display signup page with register functions
 * @param $arrayofinputs
 * @return mixed
 */
function displaySignup($arrayofinputs)
{



        if(isset($arrayofinputs["inputEmail"]) && isset($arrayofinputs["inputPassword"]) && isset($arrayofinputs["inputPasswordCheck"]))
        {
            $userEmail = $arrayofinputs["inputEmail"];
            $userPassword = hash("sha256",$arrayofinputs["inputPassword"]);
            $userRepeatPassword = hash("sha256",$arrayofinputs["inputPasswordCheck"]);
            $userAddress = $arrayofinputs["address"];
            $userCity = $arrayofinputs["locality"];
            $userZipCode = $arrayofinputs["zipcode"];
            $userPhoneNumber = $arrayofinputs["phonenumber"];

            if($userPassword == $userRepeatPassword){

                $FirstName = $arrayofinputs["inputFirstName"];
                $LastName = $arrayofinputs["inputLastName"];

                unset($_SESSION["registerError"]);



                if (isRegisterCorrect($userEmail,strtoupper($userPassword),$FirstName,$LastName,$userAddress,$userCity,$userZipCode,$userPhoneNumber))
                {

                    $_GET["action"] = "home";
                    displayHome();
                }
                else
                {

                    $_GET["action"] = "signup";
                    require "View/signup.php";
                }
            }else{
                $_SESSION["registerError"]=1;
                $_GET["action"] = "signup";
                require "View/signup.php";
            }



        }
        else
        {
            $_SESSION["registerError"]=1;
            $_GET["action"] = "signup";
            require "View/signup.php";
        }
        return $arrayofinputs;
    }

/**
 * Create Seesion for the user, used when register and login
 * @param $userEmail
 * @param $FirstName
 * @param $LastName
 * @param $userPhoneNumber
 * @param $userCity
 * @param $userZipCode
 * @param $userAddress
 * @param $userID
 */
function createSession($userEmail,$FirstName,$LastName,$userPhoneNumber,$userCity,$userZipCode,$userAddress,$userID){
        $_SESSION["userEmail"] = $userEmail;
        $_SESSION['firstname'] = $FirstName;
        $_SESSION['lastname'] = $LastName;
        $_SESSION['phonenumber'] = $userPhoneNumber;
        $_SESSION['city'] = $userCity;
        $_SESSION['zipcode'] = $userZipCode;
        $_SESSION['address'] = $userAddress;
        $_SESSION["userID"] = $userID;

}
//Displays the page with adverts
function displayAnnonce()
{

    $table = allAdverts();
    $advertCounter = countAdverts();
    require 'View/Annonce.php';

}

/**
 * Diplays the login page with the login funciton included
 * @param $loginRequest
 */
function login($loginRequest)
{


    if(isset($loginRequest["inputEmail"]) && isset($loginRequest["inputpassword"]))
    {
        $userEmail = $loginRequest["inputEmail"];

        $userPassword = hash("sha256",$loginRequest["inputpassword"]);


        if (isLoginCorrect($userEmail, $userPassword))
        {
            $_SESSION["userEmail"] = $userEmail;
            unset($_SESSION["ErrorLogin"]);
            $_GET["action"] = "home";
            displayHome();
        }
        else
        {
            $_SESSION["ErrorLogin"] = 1;
            $_GET["action"] = "login";
            require "View/login.php";
        }

    }
    else
    {

        $_GET["action"] = "home";

    }
}

//Logs out and destroys the users session
function logout()
{
    $_SESSION = array();
    session_destroy();
    $_GET["action"] = "home";
    displayHome();
}