<?php
/**
 *
 * Author   : nathan.rayburn@cpnv.ch
 * Project  : Projet Web
 * Created  : 07.02.2019

 */

function mailUser($userMail,$userMessage){
    $headers = $userMail. "\r\n" .
    $subject = "Subject : EasySearch - Mail";
    mail($_SESSION["userEmail"],$subject,$userMessage,$headers);
}
/**
 * Verify login
 * @param $userEmail
 * @param $userPassword
 * @return bool
 */
function isLoginCorrect($userEmail, $userPassword)
{
    $isLoginCorrects = false;

    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"),true);

        foreach($data as $user){

           if($user['mail'] == $userEmail &&  $user['password']== strtoupper($userPassword)){

               $isLoginCorrects = true;
               $FirstName= $user['firstname'];
               $LastName = $user['lastname'];
               $userPhoneNumber  =$user['phonenumber'];
               $userCity  = $user['locality'];
               $userZipCode=$user['zipcode'];
               $userAddress = $user['address'];
               $userID = $user['membreId'];
               createSession($userEmail,$FirstName,$LastName,$userAddress,$userZipCode,$userCity,$userPhoneNumber,$userID);

           }
        }
    }
    else
    {
        echo "Fichier non trouver";
    }


    return $isLoginCorrects;
}

/**
 * @param $userEmail
 * @param $userPassword
 * @param $FirstName
 * @param $LastName
 * @return bool
 */
function isRegisterCorrect($userEmail, $userPassword, $FirstName, $LastName,$userPhoneNumber,$userCity,$userZipCode,$userAddress){
    $writeArray = array();
    $registerCorrect = false;
    $userExist = -1;
    strtoupper($userPassword);
    //Verify if the email already exists
    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"), true);

        foreach ($data as $user) {

            if ($user['mail'] != $userEmail) {
                $userExist = 1;

            } else {
                $userExist = 0;
                echo "User Already Exists";
            }
        }


        if ($userExist == 1) {

            $counterUser = countUser();
            $counterUser ++;
            $_SESSION["userID"] = $counterUser;
            $filePath = "Model/data/example.json";
            //Put everything into a table
            $writeArray =
                array(
                    "membreId" => $counterUser,
                    "lastname" => $LastName,
                    "firstname" => $FirstName,
                    "address" => $userAddress,
                    "zipcode" => $userZipCode,
                    "locality" => $userCity,
                    "phonenumber" => $userPhoneNumber,
                    "mail" => $userEmail,
                    "password" => $userPassword,
                    "adverts" => array(),

                );


            //Write into the data base

            array_push($data, $writeArray);
            //Convert updated array to JSON
            $jsondata = json_encode($data, JSON_PRETTY_PRINT);

            //write json data into data.json file
            if (file_put_contents($filePath, $jsondata)) {
                $userID = $counterUser;
                createSession($userEmail,$FirstName,$LastName,$userPhoneNumber,$userCity,$userZipCode,$userAddress,$userID);
                $registerCorrect = true;
            } else {
                echo "Your account hasn't been saved";
                $registerCorrect = false;
            }
        }
    }

    else
    {
        echo "Fichier non trouver";
        $registerCorrect = false;
    }




    return $registerCorrect;
}

/**
 * //count the number of table users
 * @return int
 */

function countUser(){
    $counterUser = 0;

    //Count Users
    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"),true);
        foreach ($data as $user)
        {
            if($user['membreId'])
            {
                $counterUser++;
            }
        }

    }



    return $counterUser;
}

/**
 * //count the number of ads
 * @return int
 */
function countAdverts(){
    $counterAdverts = 0;
    $tableAdverts = allAdverts();
    //Count Adverts
    foreach ($tableAdverts as $singleAdverts)
    {

                $counterAdverts++;




    }
    return $counterAdverts;
}

/**
 * Cette Fonction Sert a ajouter une annonce dans la base de donnée
 * @param $dates
 * @param $title
 * @param $description
 * @param $category
 * @param $image
 * @param $price
 * @param $type
 */
function uploadAdvert($dates,$title,$description,$category,$image,$price,$type){
    //create table$
    $table = array();
    $advert = array();
    $counter = countAdverts();
    $filePath = "Model/data/example.json";
    $counterAD = -1;
    $flag = 0 ;
    $img = $image.$_FILES["imgUpload"]["name"];
    $allAds = allAdverts();
    $AdID = array();
    //autoincrementation by filtering all ids by the highest number then adding 1 to the biggest
    /*  $LASTID = 0;
    foreach($allAds as $ads){
        array_push($AdID,$ads["advertId"]);
    }
    foreach($AdID as $ID){
        if($ID > $AdID[$ID-1]){
            $LASTID = $ID;
        }
    }*/
    $LASTID =+ 3;

    //Put everything into a table
    $advert =
        array(
            "advertId" => $counterAD + 3,
            "title" => $title,
            "date" => $dates,
            "type" => $type,
            "category" => $category,
            "price" => $price,
            "image" => $img,
            "description" => $description,



        );
    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"), true);


        //on parcourt les membres
        foreach($data as $user){


                if($flag == 0){
                    $counterAD++;
                }
                if($user['membreId'] == $_SESSION["userID"]){
                    //on ajoute les annonces du membre logger dans une table
                    //Write into the data base
                    array_push($data[$counterAD]["adverts"], $advert);
                    $flag = 1;
                }

        }




        //rewrite the whole DB
        //Convert updated array to JSON
        $jsondata = json_encode($data, JSON_PRETTY_PRINT);

        //write json data into data.json file
        if (file_put_contents($filePath, $jsondata)) {

        } else {
            echo "Your account hasn't been saved";

        }



    }
    //upload the advert b finding the members id

}

/**
 * Removes 1 advert that the user has selected
 */
function removeAdvert(){

        $filePath = "Model/data/example.json";
        $table = array();
        if (file_exists("Model/data/example.json")) // the file already exists -> load it
        {
            $data = json_decode(file_get_contents("Model/data/example.json"), true);


            //on parcourt les membres
            foreach($data as $index => $user){
                foreach($user["adverts"] as $indexAd => $adverts){
                    if($adverts['advertId'] == $_GET["code"]){
                        unset($data[$index]["adverts"][$indexAd]); /*eleting the table where the table id is
                                                                    the same as the one that the user wanted to delete*/
                        $table = $data;
                    }
                }

            }

            //rewrite the whole DB
            //Convert updated array to JSON
            $jsondata = json_encode($table, JSON_PRETTY_PRINT);

            //write json data into data.json file
            if (file_put_contents($filePath, $jsondata)) {

            } else {
                echo "Your account hasn't been saved";

            }


        }


}
//Cette fonction revoie les annonces de l'utilisateur logger
/**
 * Display the Details of the Advert
 * @return array
 */
function details(){
    $table = array();
    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"), true);


        //on parcourt les membres
        foreach($data as $user){
           foreach($user["adverts"] as $adverts){
               if($adverts['advertId'] == $_GET["code"]){
                   $table =  $adverts; //on ajoute les annonces du membre logger dans une table
                   $table["memberId"] = $user["membreId"];
                   $table["lastname"] = $user["lastname"];
                   $table["firstname"] = $user["firstname"];
                   $table["mail"] = $user["mail"];
                   $table["phonenumber"] = $user["phonenumber"];
               }
           }

        }



    }
    return $table;
}
//Cette fonction revoie les annonces de l'utilisateur logger
/**
 * Returns a table with the users Adverts
 * @return array
 */
function userAdvert(){
    $table = array();
    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"), true);


        //on parcourt les membres
        foreach($data as $user){
           if($user['membreId'] == $_SESSION["userID"]){
              $table =  $user['adverts']; //on ajoute les annonces du membre logger dans une table

           }
        }



    }
    return $table;
}

/**
 * Puts all adverts into one table
 * @return array
 */
function allAdverts(){
    $table = array();
    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"), true);
        $number = countUser() - 1;

        //on parcourt les membres
        for ($i = 0; $i <= $number; $i++) {
            //on parcourt les annonces du membre
            foreach ($data[$i]['adverts'] as $advert) {
                array_push($table, $advert); //on ajoute l'annonce dans une table

            }

        }
    }
        return $table;
}

/**
 * This function returns a table with 2 recent adverts that have been added
 * @return array
 */
function advertsExport()
{
    $advertsValidated = false;
    $table = array();
    $tableRecents =array();
    $IDR1= -1;
    $IDR2 = -1;
    $DateR1 = "1950.12.31";
    $DateR2 = "1950.12.31";

    if (file_exists("Model/data/example.json")) // the file already exists -> load it
    {
        $data = json_decode(file_get_contents("Model/data/example.json"),true);
        $number = countUser()-1;

        //on parcourt les membres
        for ($i=0;$i<=$number;$i++)
        {
            //on parcourt les annonces du membre
            foreach( $data[$i]['adverts'] as $advert){
                array_push($table,$advert); //on ajoute l'annonce dans une table

            }

       }
        //Parcour un table d'annonce sans utilisateurs
        foreach($table as $advert){
            //Cas où la date de l'annonce est entre les deux dates déjà trouvées
            //Remplacer la plus ancienne
            if($advert["date"]<=$DateR1 && $advert["date"] > $DateR2){
                $DateR2 = $advert["date"];
                $IDR2 = $advert["advertId"];
            }

            //Cas où la date de l'annonce est plus récente que les deux dates trouvées
            //Décaler les deux et perdre la plus ancienne
            if($advert["date"]>=$DateR1){
                $DateR2 = $DateR1;
                $IDR2 = $IDR1;
                $DateR1 = $advert["date"];
                $IDR1 = $advert["advertId"];
            }

            //Pas besoin de traiter le cas où la date est plus ancienne ou égale à la deuxième

        }
        //Put the Recent Adverts into a table
        foreach($table as $advert){
            if($advert["advertId"] == $IDR1){
                $tableRecents["0"] = $advert;

            }
            if($advert["advertId"] == $IDR2){
                $tableRecents["1"] = $advert;
            }
        }



    }
    else
    {
        echo "Fichier non trouver";
    }


    return $tableRecents;
}


