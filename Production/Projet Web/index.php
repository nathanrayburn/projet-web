<?php
/**
 *
 * Author   : nathan.rayburn@cpnv.ch
 * Project  : Projet Web
 * Created  : 07.02.2019

 */

session_start();
require 'Controler/controler.php';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
    switch ($action) {
        case 'home':
            displayHome();
            break;
        case 'login':
            displayLogin($_POST);
            break;
        case 'signup':
            displaySignup($_POST);
            break;
        case 'annonces':
            displayAnnonce();
            break;
        case "logout":
            logout();
            break;
        case "profil":
            displayProfil();
            break;
        case "modify":
            modifyAdvert($_POST);
            break;
        case "delete":
            deleteAdvert();
            break;
        case "addAdvert":
            addAdvert($_POST);
            break;
        case "details":
            displayDetails();
            break;
        case "mail":
            sendMail($_POST);
            break;
        default :
            displayHome($_POST);
    }
    return $action;

} else {
    displayHome();

}
